package se.experis.Assignment_3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.Assignment_3.models.Franchise;

public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
