package se.experis.Assignment_3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.Assignment_3.models.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {
}
