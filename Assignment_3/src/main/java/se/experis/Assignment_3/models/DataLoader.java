package se.experis.Assignment_3.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.stereotype.Component;
import se.experis.Assignment_3.repositories.CharacterRepository;
import se.experis.Assignment_3.repositories.FranchiseRepository;
import se.experis.Assignment_3.repositories.MovieRepository;

import java.util.ArrayList;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    CharacterRepository characterRepository;

    @Autowired
    FranchiseRepository franchiseRepository;

    @Autowired
    MovieRepository movieRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        if (characterRepository.findAll().size() != 0 && franchiseRepository.findAll().size() != 0 && movieRepository.findAll().size() != 0) {
            return;
        }
        // Movie 1 Interstellar.
        Character c1 = new Character("Bruce Wayne", "Batman", "Man", "https://upload.wikimedia.org/wikipedia/commons/b/be/Batman_%28retouched%29.jpg");
        characterRepository.save(c1);
        Character c2 = new Character("Cooper", "-", "Man", "https://www.looper.com/img/gallery/the-ending-of-interstellar-explained/intro-1562880872.jpg");
        characterRepository.save(c2);
        Character c3 = new Character("Cobb", "-", "Man", "https://s3.amazonaws.com/thumbnails.thecrimson.com/photos/2020/10/26/232756_1346454.jpg.1500x999_q95_crop-smart_upscale.jpg");
        characterRepository.save(c3);
        Character c4 = new Character("Amelia Brand", "-", "Woman", "https://static.wikia.nocookie.net/interstellarfilm/images/d/d9/Ameliabrand1.jpg/revision/latest?cb=20140606063903");
        characterRepository.save(c4);

        Franchise f1 = new Franchise("C.Nolan", "Nolan");
        franchiseRepository.save(f1);

        Franchise f2 = new Franchise("DC", "Batman");
        franchiseRepository.save(f2);

        Movie m1 = new Movie("Interstellar", "Sci-Fi/Thriller", "2014",
                "C.Nolan", "https://images-na.ssl-images-amazon.com/images/S/pv-target-images/f56404b83302e92575989e264f20f22a77338a03ac8609e338ce107f7da5d08d._RI_V_TTW_.jpg",
                "https://www.youtube.com/watch?v=0vxOhd4qlnA");
        m1.setFranchise(f1);
        ArrayList<Character> m1List = new ArrayList<>();
        m1List.add(c2);
        m1List.add(c4);
        m1.setCharacters(m1List);
        movieRepository.save(m1);

        Movie m2 = new Movie("Inception", "Sci-Fi/Thriller", "2010", "C.Nolan", "https://militz.blob.core.windows.net/v-3/movies/27205/inception-p.jpg",
                "https://www.youtube.com/watch?v=9ttJzvPYM8M");
        m2.setFranchise(f1);
        ArrayList<Character> m2List = new ArrayList<>();
        m2List.add(c3);
        m2.setCharacters(m2List);
        movieRepository.save(m2);

        Movie m3 = new Movie("Batman Begins", "Action", "2005", "C.Nolan", "https://sfanytime-images-prod.secure.footprint.net/COVERM/ba242472-c422-41dd-a724-9f81010f54bf_COVERM_01." +
                "jpg?w=375&fm=pjpg&s=cd8710e230dbe303f7f9795a38beba57", "https://www.youtube.com/watch?v=p9sgFTFvHds");
        m3.setFranchise(f2);
        ArrayList<Character> m3List = new ArrayList<>();
        m3List.add(c1);
        m3.setCharacters(m3List);
        movieRepository.save(m3);
    }
}
