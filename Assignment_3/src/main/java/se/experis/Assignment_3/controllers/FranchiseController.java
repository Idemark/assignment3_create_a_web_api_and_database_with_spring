package se.experis.Assignment_3.controllers;


import org.hibernate.collection.internal.PersistentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.Assignment_3.models.Character;
import se.experis.Assignment_3.models.Franchise;
import se.experis.Assignment_3.models.Movie;
import se.experis.Assignment_3.repositories.FranchiseRepository;
import se.experis.Assignment_3.repositories.MovieRepository;

import java.util.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchise")
public class FranchiseController {
    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    /*
    Gets all the franchises by using the findAll() on the franchiseRepository, which sends a query request for all franchises.
     */
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    /*
    Gets a specific franchise by using the findById(Long) on the franchiseRepository. If the franchise exists
    it sends it to the one requested.
    */
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    /*
    Gets all the movies that are in a specific franchise. If the franshise exist and if the list of movies in the specific
    franchise isn't null/empty. All the movies are added the a list and then sent.
     */
    @GetMapping("/allMovies/{id}")
    public ResponseEntity<List<Movie>> getAllMoviesInFranchise(@PathVariable Long id) {
        List<Movie> movieList = movieRepository.findAll();
        List<Movie> returnMovieList = new ArrayList<>();
        HttpStatus status = null;
        if (franchiseRepository.existsById(id)) {
            if (movieList != null) {

                for (int i = 0; i < movieList.size(); i++) {
                    if (movieList.get(i).getFranchise().getId() == id) {
                        returnMovieList.add(movieList.get(i));
                        status = HttpStatus.OK;
                    }
                }
            } else {
                status = HttpStatus.NOT_FOUND;
            }
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovieList, status);
    }

    /*
    Gets all the characters that are in a specific franchise. If the franchise exist and if the list of movies in the specific
    franchise isn't null/empty. It takes all the characters in the movies and adds i the a list. If the character already are added
    from one movie it isn't added again if it appears in another.
     */
    @GetMapping("/allCharacters/{id}")
    public ResponseEntity<List<Character>> getAllCharactersInFranchise(@PathVariable Long id) {
        List<Movie> movieList = movieRepository.findAll();
        List<Movie> moviesInFranchise = new ArrayList<>();
        List<Character> returnCharacterList = new ArrayList<>();
        List<Long> ids = new ArrayList<>(); // list that holds all the added characters by id
        HttpStatus status = null;
        if (franchiseRepository.existsById(id)) {
            if (movieList != null) {

                for (int i = 0; i < movieList.size(); i++) {
                    if (movieList.get(i).getFranchise().getId() == id) {
                        moviesInFranchise.add(movieList.get(i));
                    }
                }
                if (moviesInFranchise.size() > 0) {
                    status = HttpStatus.OK;
                    //nested loop to be able to get all characters in every single movie inside the franchise.
                    for (int i = 0; i < moviesInFranchise.size(); i++) {
                        for (int j = 0; j < moviesInFranchise.get(i).getCharacters().size(); j++) {
                            if (!ids.contains(moviesInFranchise.get(i).getCharacters().get(j).getId())) { // checks if the character already added.
                                ids.add(moviesInFranchise.get(i).getCharacters().get(j).getId());
                                returnCharacterList.add(moviesInFranchise.get(i).getCharacters().get(j));
                            }
                        }
                    }
                } else {
                    status = HttpStatus.NOT_FOUND;
                }
            }
        }
        return new ResponseEntity<>(returnCharacterList, status);
    }

    /*
    Using post to be able to add a new franchise object to the database. The save() sends the franchise to the repository and
    it sends it to the database.
     */
    @PostMapping()
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        HttpStatus status;
        franchise = franchiseRepository.save(franchise);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(franchise, status);
    }

    /*
    Using put to be able to update a franchise object to the database. The save() sends the updated franchise to the repository and
    it sends it to the database.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if (!id.equals(franchise.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /*
    Updates which movies that should be inside a specific franchise with a put request. By sending in a list of movieIds that
    the franchise should have.
     */
    @PutMapping("/movies/{id}")
    public ResponseEntity<Franchise> updateMoviesInFranchise(@PathVariable Long id, @RequestBody List<Long> ids) {
        ArrayList<Movie> movieArrayList = new ArrayList<>();
        Franchise returnFranchise = new Franchise();
        HttpStatus status = null;

        if (franchiseRepository.existsById(id)) {
            returnFranchise = franchiseRepository.findById(id).get();
            for (int i = 0; i < ids.size(); i++) {
                Movie returnMovie = new Movie();
                if (movieRepository.existsById(ids.get(i))) {
                    status = HttpStatus.OK;
                    returnMovie = movieRepository.findById(ids.get(i)).get();
                    movieArrayList.add(returnMovie);
                } else {
                    status = HttpStatus.NOT_FOUND;
                    return new ResponseEntity<>(returnFranchise, status);
                }
            }
        }
        returnFranchise.setMovies(movieArrayList);
        franchiseRepository.save(returnFranchise);
        return new ResponseEntity<>(returnFranchise, status);
    }

    /*
    Deletes a franchise by id, using the method deleteById().
     */
    @PutMapping("/delete/{id}")
    public ResponseEntity<Franchise> deleteFranchiseById(@PathVariable Long id) {
        HttpStatus status = null;
        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            franchiseRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(status);
    }
}
