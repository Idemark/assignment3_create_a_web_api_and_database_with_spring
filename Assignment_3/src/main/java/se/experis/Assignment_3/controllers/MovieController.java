package se.experis.Assignment_3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.Assignment_3.models.Character;
import se.experis.Assignment_3.models.Franchise;
import se.experis.Assignment_3.models.Movie;
import se.experis.Assignment_3.repositories.CharacterRepository;
import se.experis.Assignment_3.repositories.FranchiseRepository;
import se.experis.Assignment_3.repositories.MovieRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController {
    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    /*
    Gets all the movies by using the findAll() on the movieRepository, which sends a query request for all movies.
     */
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    /*
    Gets a specific movie by using the findById(Long) on the movieRepository. If the movie exists
    it sends it to the one requested.
    */
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    /*
    Gets all the characters that are in a specific movie. If the movie exist and if the list of character in the specific
    movie isn't null/empty. All the character are added the a list and then sent.
     */
    @GetMapping("/characters/{id}")
    public ResponseEntity<List<Character>> getAllCharactersInMovie(@PathVariable Long id) {
        List<Character> returnCharacterList = new ArrayList<>();
        Movie returnMovie = new Movie();
        HttpStatus status = null;
        if (movieRepository.existsById(id)) {
            if (returnMovie.getCharacters() != null) {
                status = HttpStatus.OK;
                returnMovie = movieRepository.findById(id).get();
                for (int i = 0; i < returnMovie.getCharacters().size(); i++) {
                    returnCharacterList.add(returnMovie.getCharacters().get(i));
                }
            }
        }
        return new ResponseEntity<>(returnCharacterList, status);
    }

    /*
    Using post to be able to add a new movie object to the database. The save() sends the movie to the repository and
    it sends it to the database.
     */

    @PostMapping()
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        HttpStatus status;
        movie = movieRepository.save(movie);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    /*
    Using put to be able to update a movie object to the database. The save() sends the updated movie to the repository and
    it sends it to the database.
     */

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        if (!id.equals(movie.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    /*
    Updates which characters that should be inside a specific movie with a put request. By sending in a list of characterIds that
    the movie should have.
     */
    @PutMapping("/characters/{id}")
    public ResponseEntity<Movie> updateCharactersInMovie(@RequestBody List<Long> ids, @PathVariable Long id) {
        ArrayList<Character> characterArrayList = new ArrayList<>();
        Movie returnMovie = new Movie();
        HttpStatus status = null;
        if (movieRepository.existsById(id)) {
            returnMovie = movieRepository.findById(id).get();
            for (int i = 0; i < ids.size(); i++) {
                Character returnCharacter = new Character();
                if (characterRepository.existsById(ids.get(i))) {
                    status = HttpStatus.OK;
                    returnCharacter = characterRepository.findById(ids.get(i)).get();
                    characterArrayList.add(returnCharacter);
                } else {
                    status = HttpStatus.NOT_FOUND;
                    return new ResponseEntity<>(returnMovie, status);
                }
            }
        }
        returnMovie.setCharacters(characterArrayList);
        movieRepository.save(returnMovie);
        return new ResponseEntity<>(returnMovie, status);
    }

    /*
    Deletes a movie by id, using the method deleteById().
    */
    @PutMapping("/delete/{id}")
    public ResponseEntity<Movie> deleteMovieById(@PathVariable Long id) {
        HttpStatus status = null;
        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            movieRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(status);
    }
}
