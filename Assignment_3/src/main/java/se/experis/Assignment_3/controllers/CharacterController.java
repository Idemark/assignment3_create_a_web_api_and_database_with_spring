package se.experis.Assignment_3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.Assignment_3.models.Character;
import se.experis.Assignment_3.models.Franchise;
import se.experis.Assignment_3.repositories.CharacterRepository;
import se.experis.Assignment_3.repositories.MovieRepository;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;
    @Autowired
    private MovieRepository movieRepository;

    /*
    Gets all the characters by using the findAll() on the characterRepository. If the franchise exists
    it sends it to the one requested.
     */
    @GetMapping()
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    /*
    Gets a specific character by id using the findById(Long) on the characterRepository, which sends a query request to find and send back
    the specific character.
    */
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacter(@PathVariable Long id) {
        Character returnCharacter = new Character();
        HttpStatus status;

        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, status);
    }

    /*
    Using post to be able to add a new character object to the database. The save() sends the character to the repository and
    it sends it to the database.
    */
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        HttpStatus status;
        character = characterRepository.save(character);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(character, status);
    }

    /*
    Request a character and overwrites it using the save() function on the characterRepository, to be able to update it.
    If the id is wrong it sends back a not_found.
    */
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character) {
        Character returnCharacter = new Character();
        HttpStatus status;

        if (!id.equals(character.getId())) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnCharacter, status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /*
    Deletes a character by id, using the method deleteById().
    */
    @PutMapping("/delete/{id}")
    public ResponseEntity<Character> deleteCharacterById(@PathVariable Long id) {
        HttpStatus status = null;
        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            characterRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(status);
    }

}
