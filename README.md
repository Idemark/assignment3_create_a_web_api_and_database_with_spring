# Create a web API and database with spring

## Intro

For this assignment the goal were to build a database containing characters, movies they appear in and franchises those movies belongs to. The application are constructed in Spring Web and comprise of a database made in PostgreSQL through Hibernate with a RESTFUL API to allow users to manipulate the data. The APIs requirements were to give full CRUD (Create,read,update,delete). Swagger UI was used to visualize and interact with the API.

## Project structure

The project includes three packages controllers, models and repositories. The controller package have three classes (CharacterController, FranchiseController and MovieController). These controllers holds the endpoints for the different entities. The models hold all the information about each entity and the relationships between them. It also contains a class called Dataloader that seeds in data to the database when starting up the program. The repositories extends the JPArepository that handles the queries.  

![ER-digram](ERdiagram.png)


## Members
Katalina Palma, Alexander Idemark
